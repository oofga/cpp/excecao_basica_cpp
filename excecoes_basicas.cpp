#include <iostream>

using namespace std;

int testaExcecao(void) {
   int valor;
   cout << "Insira um valor: ";
   cin >> valor;

   if (valor < 0) {
      //thorw(1); // Int      
      //throw('a');
      throw(1.0);
   } 
   else
      return valor;
}
int main (int argc, char ** argv) {

   int valor;

   try{
     valor = testaExcecao();
     cout << "Valor = " << valor << endl;
   }
   catch(int e){
     cout << "Exceção: Valor negativo" << endl;
   }
   catch(char e){
     cout << "Exceção do tipo char" << endl;
   }
   catch(...){
     cout << "Exceção Genérica" << endl;
   }

   cout << "Término do Programa" << endl;
   return 0;
}



